
import java.util.*;

public class ListSort {
    public final static void main(String... args) {
        //Ordenando y mostrando lista de enteros
        showList(sortList(Arrays.asList(new Integer[] { 50, 100, 12, 5, 20, 69 })));
        //Ordenando y mostrando lista de Caracteres
        showList(sortList(Arrays.asList(new Character[] { 'L', 'X', 'Z', 'Q', 'E', 'P' })));
        //Ordenando y mostrando lista de Parent (Objeto definido por el Programador)
        showList(sortList(Arrays.asList(
            new Parent[] { 
                new Parent().set(10l), 
                new Parent().set(100l), 
                new Parent().set(80l),
                new Parent().set(13l) 
        })));
    }

    /**
     * Método que recibe una lista de objetos que implementan la interfaz
     * Comparable
     * 
     * @param <T>  Generico
     * @param list Lista a ordenar
     * @return retorna la lista ordenada
     */
    public  static <T extends Comparable<T>> List<T> sortList(List<T> list){
        int n = list.size();
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (list.get(j).compareTo(list.get(j + 1)) > 0) {
                    T temp = list.get(j);
                    list.set(j, list.get(j + 1));
                    list.set(j + 1, temp);
                }
        return list;
    }

    /**
     * Imprime la lista
     * 
     * @param <T>  Generico
     * @param list Lista a imprimir
     */
    public static <T extends Comparable> void showList(List<T> list){
        System.out.printf("\n===============LISTA TIPO %s=========================\n", list.getClass());
        for (T p: list) System.out.println(p);
        System.out.println("==========================================================");
    }

    /**
     * Clase que para ejemplificar que con objetos propios funcionan los metodos
     */
    private static class Parent implements Comparable<Parent> {
        private Long posicion = 0L;
        private String parentName = "Parent";

        public Parent set(long v) {
            this.posicion = v;
            return this;
        }

        public String toString() {
            return this.parentName + " " + this.posicion;
        }

        public int compareTo(Parent o) {
            return this.posicion > o.posicion ? 1 : -1;
        }
    }
}